<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//back end


Route::get('/admin', function () {
    return view('admin/index');
});

//Front page

Route::get('/8-dresses', function () {
    return view('front.8-dresses');
});
Route::get('/4-tops', function () {
    return view('front.4-tops');
});
Route::get('/3-women', function () {
    return view('front.3-women');
});
Route::get('/46-pants', function () {
    return view('front.8-dresses');
});
Route::get('/8-dresses', function () {
    return view('front.8-dresses');
});Route::get('/8-dresses', function () {
    return view('front.8-dresses');
});Route::get('/8-dresses', function () {
    return view('front.8-dresses');
});Route::get('/8-dresses', function () {
    return view('front.8-dresses');
});Route::get('/8-dresses', function () {
    return view('front.8-dresses');
});



///admin login-end
Route::get('/admin','adminController@AdminIndex');

Route::get('/logout','adminController@logout');

Route::get('/admin-panel','adminController@index');

Route::post('/admin-login-check','adminController@auth_check');