@include('front.layouts.head-semi')
			<div class="columns-container">
				<div id="columns" class="container">

<!-- Breadcrumb -->
            <div class="breadcrumb clearfix">
	<a class="home" href="http://kute-themes.com/prestashop/supershop/option1/" title="Return to Home">Home</a>
			<span class="navigation-pipe" >&nbsp;</span>
					<a href="12-fashion.html" title="Fashion" data-gg="">Fashion</a><span class="navigation-pipe">></span><span class="navigation_page"><a href="3-women.html" title="Women" data-gg="">Women</a><span class="navigation-pipe">></span>Tops</span>
			</div>
<!-- /Breadcrumb -->


					<div class="row">
												<div id="left_column" class="column col-xs-12 col-sm-3">


<!-- Block layered navigation module -->


<div id="layered_block_left" class="block">

	<p class="title_block">Filter selection</p>

	<div class="block_content">

		<form action="#" id="layered_form">

			<div>





							<div class="layered_filter ">


                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Categories</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_category_0"></a>

                            </span>

						</div>

						<ul id="ul_layered_category_0" class="col-lg-12 layered_filter_ul">





										<li class="nomargin hiddable col-lg-6 ">


												<input type="checkbox" class="checkbox" name="layered_category_5" id="layered_category_5" value="5" />


											<label
												for="layered_category_5"
																																																>




																											<a href="4-tops.html#categories-t_shirts" data-rel="nofollow">T-shirts<span> (2)</span></a>



											</label>

										</li>








						</ul>

					</div>





							<div class="layered_price" style="display: none;">


                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Price</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_price_0"></a>

                            </span>

						</div>

						<ul id="ul_layered_price_0" class="col-lg-12 layered_filter_ul">



									<li>

									<label >

										Range:

									</label>

									<span id="layered_price_range"></span>

									<div class="layered_slider_container">

										<div class="layered_slider" id="layered_price_slider" data-type="price" data-format="2" data-unit="€"></div>

									</div>

									</li>




						</ul>

					</div>





							<div class="layered_filter color_class  size_class">


                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Color</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_id_attribute_group_3"></a>

                            </span>

						</div>

						<ul id="ul_layered_id_attribute_group_3" class="col-lg-12 layered_filter_ul color-group">





										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#AAB2BD" type="button" name="layered_id_attribute_group_5" data-rel="5_3" id="layered_id_attribute_group_5"  style="background: #AAB2BD;" />



											<label
												for="layered_id_attribute_group_5"

														class="layered_color " data-rel="5_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#CFC4A6" type="button" name="layered_id_attribute_group_6" data-rel="6_3" id="layered_id_attribute_group_6"  style="background: #CFC4A6;" />



											<label
												for="layered_id_attribute_group_6"

														class="layered_color " data-rel="6_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#ffffff" type="button" name="layered_id_attribute_group_8" data-rel="8_3" id="layered_id_attribute_group_8"  style="background: #ffffff;" />



											<label
												for="layered_id_attribute_group_8"

														class="layered_color " data-rel="8_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#E84C3D" type="button" name="layered_id_attribute_group_10" data-rel="10_3" id="layered_id_attribute_group_10"  style="background: #E84C3D;" />



											<label
												for="layered_id_attribute_group_10"

														class="layered_color " data-rel="10_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#434A54" type="button" name="layered_id_attribute_group_11" data-rel="11_3" id="layered_id_attribute_group_11"  style="background: #434A54;" />



											<label
												for="layered_id_attribute_group_11"

														class="layered_color " data-rel="11_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#C19A6B" type="button" name="layered_id_attribute_group_12" data-rel="12_3" id="layered_id_attribute_group_12"  style="background: #C19A6B;" />



											<label
												for="layered_id_attribute_group_12"

														class="layered_color " data-rel="12_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#F39C11" type="button" name="layered_id_attribute_group_13" data-rel="13_3" id="layered_id_attribute_group_13"  style="background: #F39C11;" />



											<label
												for="layered_id_attribute_group_13"

														class="layered_color " data-rel="13_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#5D9CEC" type="button" name="layered_id_attribute_group_14" data-rel="14_3" id="layered_id_attribute_group_14"  style="background: #5D9CEC;" />



											<label
												for="layered_id_attribute_group_14"

														class="layered_color " data-rel="14_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#A0D468" type="button" name="layered_id_attribute_group_15" data-rel="15_3" id="layered_id_attribute_group_15"  style="background: #A0D468;" />



											<label
												for="layered_id_attribute_group_15"

														class="layered_color " data-rel="15_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#F1C40F" type="button" name="layered_id_attribute_group_16" data-rel="16_3" id="layered_id_attribute_group_16"  style="background: #F1C40F;" />



											<label
												for="layered_id_attribute_group_16"

														class="layered_color " data-rel="16_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#964B00" type="button" name="layered_id_attribute_group_17" data-rel="17_3" id="layered_id_attribute_group_17"  style="background: #964B00;" />



											<label
												for="layered_id_attribute_group_17"

														class="layered_color " data-rel="17_3"
																																				>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input class="color-option  " value="#FCCACD" type="button" name="layered_id_attribute_group_24" data-rel="24_3" id="layered_id_attribute_group_24"  style="background: #FCCACD;" />



											<label
												for="layered_id_attribute_group_24"

														class="layered_color " data-rel="24_3"
																																				>



											</label>

										</li>






						</ul>

					</div>





							<div class="layered_filter  manufacturer_class">


                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Manufacturer</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_manufacturer_0"></a>

                            </span>

						</div>

						<ul id="ul_layered_manufacturer_0" class="col-lg-12 layered_filter_ul">





										<li class="nomargin hiddable col-lg-6 ">


												<input type="checkbox" class="checkbox" name="layered_manufacturer_15" id="layered_manufacturer_15" value="15" />


											<label
												for="layered_manufacturer_15"
																																																>




																											<a href="4-tops.html#manufacturer-channelo" data-rel="nofollow">Channelo<span> (1)</span></a>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input type="checkbox" class="checkbox" name="layered_manufacturer_21" id="layered_manufacturer_21" value="21" />


											<label
												for="layered_manufacturer_21"
																																																>




																											<a href="4-tops.html#manufacturer-mamypokon" data-rel="nofollow">Mamypokon<span> (1)</span></a>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input type="checkbox" class="checkbox" name="layered_manufacturer_22" id="layered_manufacturer_22" value="22" />


											<label
												for="layered_manufacturer_22"
																																																>




																											<a href="4-tops.html#manufacturer-pradano" data-rel="nofollow">Pradano<span> (3)</span></a>



											</label>

										</li>




										<li class="nomargin hiddable col-lg-6 ">


												<input type="checkbox" class="checkbox" name="layered_manufacturer_30" id="layered_manufacturer_30" value="30" />


											<label
												for="layered_manufacturer_30"
																																																>




																											<a href="4-tops.html#manufacturer-pumano" data-rel="nofollow">Pumano<span> (1)</span></a>



											</label>

										</li>






						</ul>

					</div>



			</div>

			<input type="hidden" name="id_category_layered" value="4" />




































		</form>

	</div>

	<div id="layered_ajax_loader" style="display: none;">

		<p>

			<img src="../img/loader.gif" alt="" />

			<br />Loading...

		</p>

	</div>

</div>


<!-- /Block layered navigation module -->




<div id="htmlcontent_left">
	<ul class="htmlcontent-home clearfix row">
									<li class="htmlcontent-item-1 col-xs-12">
																					<img src="../modules/themeconfigurator/img/cfa4c95287277d00d81531c29169b6fa21e46a1e_banner.png" class="item-img img-responsive" title="" alt=""  />
																											</li>
			</ul>
</div>

	<!-- Block CMS module -->


		<section id="informations_block_left_1" class="block informations_block_left">

			<h2 class="title_block">

				<a href="content/category/1-home.html">

					Information
				</a>

			</h2>

			<div class="block_content list-block">

				<ul>




							<li>

								<a href="content/1-delivery.html" title="Delivery">

									Delivery

								</a>

							</li>




							<li>

								<a href="content/2-legal-notice.html" title="Legal Notice">

									Legal Notice

								</a>

							</li>




							<li>

								<a href="content/3-terms-and-conditions-of-use.html" title="Terms and conditions of use">

									Terms and conditions of use

								</a>

							</li>




							<li>

								<a href="content/4-about-us.html" title="About us">

									About us

								</a>

							</li>




							<li>

								<a href="content/5-secure-payment.html" title="Secure payment">

									Secure payment

								</a>

							</li>




				</ul>

			</div>

		</section>


	<!-- /Block CMS module -->


</div>
																		<div id="center_column" class="center_column col-xs-12 col-sm-9">





		<!-- Subcategories -->
		<div id="subcategories">
			<ul class="clearfix">
							<li>
					<a href="5-tshirts.html" title="T-shirts" >T-shirts</a>
				</li>
							<li>
					<a href="7-blouses.html" title="Blouses" >Blouses</a>
				</li>
						</ul>
                            <div id="category_description_full" class="unvisible rte"><p>Choose from t-shirts, tops, blouses, short sleeves, long sleeves, tank tops, 3/4 sleeves and more.</p>
<p>Find the cut that suits you the best!</p></div>
            		</div>



        <div class="view-product-list">
            <h1 class="page-heading product-listing"><span class="cat-name">Tops&nbsp;</span></h1>
            <ul class="display hidden-xs">

    <li class="view_as_grid"><a rel="nofollow" href="#" title="Grid"><i class="icon-th-large"></i>Grid</a></li>
    <li class="view_as_list"><a rel="nofollow" href="#" title="List"><i class="icon-th-list"></i>List</a></li>
</ul>

        </div>





    <input type="hidden" class="case-width" value="normal-width" />
	<!-- Products list -->
	<ul class="product_list grid row">




                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line last-item-of-mobile-line ">

                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="fashion/8-red-craft.html" title="Red craft" itemprop="url">
							<img class="replace-2x img-responsive" src="../86-home_default/red-craft.jpg" alt="Women&#039;s Woolen" title="Women&#039;s Woolen"  width="230" height="276" itemprop="image" />

						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										19,81 €
									</span>


											<span class="price-percent-reduction">20%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										15,85 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />



															</div>
																			<a class="new-box" href="fashion/8-red-craft.html">
								<span class="new-label">New</span>
							</a>
																			<a class="sale-box" href="fashion/8-red-craft.html">
								<span class="sale-label">Sale!</span>
							</a>

                        <div class="functional-buttons clearfix">

<div class="wishlist">
	<a class="addToWishlist wishlistProd_8" title="Add to my wishlist" href="javascript:void(0);" data-wl="8" onclick="WishlistCart('wishlist_block_list', 'add', '8', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="fashion/8-red-craft.html" data-id-product="8"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="fashion/8-red-craft.html" rel="fashion/8-red-craft.html">
    							<i class="fa fa-search"></i>
    						</a>



                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="orderc4c4.html?add=1&amp;id_product=8&amp;ipa=1856&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="1856" data-id-product="8" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>


    					</div>
					</div>




				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="fashion/8-red-craft.html" title="Red craft" itemprop="url" >
							Red craft
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Quisque malesuada placerat nisl.
					</p>
                    <p class="product-desc-list" itemprop="description">
						Quisque malesuada placerat nisl. Cras dapibus. Vestibulum ullamcorper mauris at ligula. Praesent adipiscing. Nullam cursus lacinia erat.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								15,85 €							  </span>

								<span class="old-price product-price">
									19,81 €
								</span>

																	<span class="price-percent-reduction">20%<span>OFF</span></span>



											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "0" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">1</span> s)</span>
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																					</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#12908675</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>




                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line last-item-of-mobile-line ">

                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="sports/20-sexy-women-t-shirt.html" title="Sexy women t-shirt" itemprop="url">
							<img class="replace-2x img-responsive" src="../82-home_default/sexy-women-t-shirt.jpg" alt="Women&#039;s Woolen" title="Women&#039;s Woolen"  width="230" height="276" itemprop="image" />

						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										59,52 €
									</span>
                                                                        <span itemprop="price" class="price product-price">
										59,52 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />



															</div>
																			<a class="new-box" href="sports/20-sexy-women-t-shirt.html">
								<span class="new-label">New</span>
							</a>

                        <div class="functional-buttons clearfix">

<div class="wishlist">
	<a class="addToWishlist wishlistProd_20" title="Add to my wishlist" href="javascript:void(0);" data-wl="20" onclick="WishlistCart('wishlist_block_list', 'add', '20', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="sports/20-sexy-women-t-shirt.html" data-id-product="20"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="sports/20-sexy-women-t-shirt.html" rel="sports/20-sexy-women-t-shirt.html">
    							<i class="fa fa-search"></i>
    						</a>



                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order3207.html?add=1&amp;id_product=20&amp;ipa=258&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="258" data-id-product="20" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>


    					</div>
					</div>




				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="sports/20-sexy-women-t-shirt.html" title="Sexy women t-shirt" itemprop="url" >
							Sexy women t-shirt
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Vestibulum eu odio. Suspendisse
					</p>
                    <p class="product-desc-list" itemprop="description">
						Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien. Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								59,52 €							  </span>



											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "3.5" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">2</span> s)</span>
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																														</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#453217907</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>




                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line first-item-of-tablet-line last-item-of-mobile-line ">

                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/7-sexy-women-blouse.html" title="Sexy women blouse" itemprop="url">
							<img class="replace-2x img-responsive" src="../221-home_default/sexy-women-blouse.jpg" alt="Printed Chiffon Dress" title="Printed Chiffon Dress"  width="230" height="276" itemprop="image" />

						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										24,60 €
									</span>


											<span class="price-percent-reduction">20%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										19,68 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />



															</div>
																			<a class="new-box" href="women/7-sexy-women-blouse.html">
								<span class="new-label">New</span>
							</a>

                        <div class="functional-buttons clearfix">

<div class="wishlist">
	<a class="addToWishlist wishlistProd_7" title="Add to my wishlist" href="javascript:void(0);" data-wl="7" onclick="WishlistCart('wishlist_block_list', 'add', '7', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/7-sexy-women-blouse.html" data-id-product="7"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/7-sexy-women-blouse.html" rel="women/7-sexy-women-blouse.html">
    							<i class="fa fa-search"></i>
    						</a>



                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order0f19.html?add=1&amp;id_product=7&amp;ipa=2278&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="2278" data-id-product="7" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>


    					</div>
					</div>




				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/7-sexy-women-blouse.html" title="Sexy women blouse" itemprop="url" >
							Sexy women blouse
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Printed chiffon knee length dress
					</p>
                    <p class="product-desc-list" itemprop="description">
						Printed chiffon knee length dress with tank straps. Deep v-neckline.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								19,68 €							  </span>

								<span class="old-price product-price">
									24,60 €
								</span>

																	<span class="price-percent-reduction">20%<span>OFF</span></span>



											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "5" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">4</span> s)</span>
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#123689833</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>




                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line last-line last-item-of-tablet-line last-item-of-mobile-line ">

                                                                                            			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/47-sexy-blouse2.html" title="Sexy blouse2" itemprop="url">
							<img class="replace-2x img-responsive" src="../304-home_default/sexy-blouse2.jpg" alt="Sexy blouse2" title="Sexy blouse2"  width="230" height="276" itemprop="image" />

						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										24,60 €
									</span>


											<span class="price-percent-reduction">20%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										19,68 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />



															</div>
																			<a class="new-box" href="women/47-sexy-blouse2.html">
								<span class="new-label">New</span>
							</a>

                        <div class="functional-buttons clearfix">

<div class="wishlist">
	<a class="addToWishlist wishlistProd_47" title="Add to my wishlist" href="javascript:void(0);" data-wl="47" onclick="WishlistCart('wishlist_block_list', 'add', '47', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/47-sexy-blouse2.html" data-id-product="47"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/47-sexy-blouse2.html" rel="women/47-sexy-blouse2.html">
    							<i class="fa fa-search"></i>
    						</a>



                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order6d58.html?add=1&amp;id_product=47&amp;ipa=1898&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="1898" data-id-product="47" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>


    					</div>
					</div>




				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/47-sexy-blouse2.html" title="Sexy blouse2" itemprop="url" >
							Sexy blouse2
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Printed chiffon knee length dress
					</p>
                    <p class="product-desc-list" itemprop="description">
						Printed chiffon knee length dress with tank straps. Deep v-neckline.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								19,68 €							  </span>

								<span class="old-price product-price">
									24,60 €
								</span>

																	<span class="price-percent-reduction">20%<span>OFF</span></span>



											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "4.5" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">6</span> s)</span>
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#12556773</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>




                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-line first-item-of-tablet-line last-item-of-mobile-line ">

                                                                                            			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/48-gentle-silk-top.html" title="Gentle silk top" itemprop="url">
							<img class="replace-2x img-responsive" src="../240-home_default/gentle-silk-top.jpg" alt="Printed Chiffon Dress" title="Printed Chiffon Dress"  width="230" height="276" itemprop="image" />

						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										24,60 €
									</span>


											<span class="price-percent-reduction">25%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										18,45 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />



															</div>
																			<a class="new-box" href="women/48-gentle-silk-top.html">
								<span class="new-label">New</span>
							</a>

                        <div class="functional-buttons clearfix">

<div class="wishlist">
	<a class="addToWishlist wishlistProd_48" title="Add to my wishlist" href="javascript:void(0);" data-wl="48" onclick="WishlistCart('wishlist_block_list', 'add', '48', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/48-gentle-silk-top.html" data-id-product="48"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/48-gentle-silk-top.html" rel="women/48-gentle-silk-top.html">
    							<i class="fa fa-search"></i>
    						</a>



                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order367c.html?add=1&amp;id_product=48&amp;ipa=804&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="804" data-id-product="48" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>


    					</div>
					</div>




				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/48-gentle-silk-top.html" title="Gentle silk top" itemprop="url" >
							Gentle silk top
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Printed chiffon knee length dress
					</p>
                    <p class="product-desc-list" itemprop="description">
						Printed chiffon knee length dress with tank straps. Deep v-neckline.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								18,45 €							  </span>

								<span class="old-price product-price">
									24,60 €
								</span>

																	<span class="price-percent-reduction">25%<span>OFF</span></span>



											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "4.3" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">10</span> s)</span>
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#14536773</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>




                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line last-line last-item-of-tablet-line last-item-of-mobile-line last-mobile-line ">

                                                                                            			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/49-light-blue-dress.html" title="Light blue dress" itemprop="url">
							<img class="replace-2x img-responsive" src="../246-home_default/light-blue-dress.jpg" alt="Blue night dress" title="Blue night dress"  width="230" height="276" itemprop="image" />

						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										36,60 €
									</span>
                                                                        <span itemprop="price" class="price product-price">
										36,60 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />



															</div>
																			<a class="new-box" href="women/49-light-blue-dress.html">
								<span class="new-label">New</span>
							</a>

                        <div class="functional-buttons clearfix">

<div class="wishlist">
	<a class="addToWishlist wishlistProd_49" title="Add to my wishlist" href="javascript:void(0);" data-wl="49" onclick="WishlistCart('wishlist_block_list', 'add', '49', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/49-light-blue-dress.html" data-id-product="49"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/49-light-blue-dress.html" rel="women/49-light-blue-dress.html">
    							<i class="fa fa-search"></i>
    						</a>



                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="ordera940.html?add=1&amp;id_product=49&amp;ipa=1919&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="1919" data-id-product="49" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>


    					</div>
					</div>




				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/49-light-blue-dress.html" title="Light blue dress" itemprop="url" >
							Light blue dress
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Sleeveless knee-length chiffon
					</p>
                    <p class="product-desc-list" itemprop="description">
						Sleeveless knee-length chiffon dress. V-neckline with elastic under the bust lining.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								36,60 €							  </span>



											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "4.6" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">9</span> s)</span>
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																														</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#168932232</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
		</ul>





			<div class="content_sortPagiBar">
                <div class="sortPagiBar clearfix">
                    	<form method="post" action="http://kute-themes.com/prestashop/supershop/option1/en/products-comparison" class="compare-form">
		<button type="submit" class="btn btn-default button button-medium bt_compare bt_compare" disabled="disabled">
			<span>Compare (<strong class="total-compare-val">0</strong>)<i class="icon-chevron-right right"></i></span>
		</button>
		<input type="hidden" name="compare_product_count" class="compare_product_count" value="0" />
		<input type="hidden" name="compare_product_list" class="compare_product_list" value="" />
	</form>







												<!-- Pagination -->
	<div id="pagination_bottom" class="pagination clearfix">
	    			</div>
    <div class="product-count">
    	        	                        	                        	Showing 1 - 6 of 6 items
		    </div>
	<!-- /Pagination -->

                    							<!-- nbr product/page -->
		<!-- /nbr product/page -->



<form id="productsSortForm" action="http://kute-themes.com/prestashop/supershop/option1/en/4-tops" class="productsSortForm">
	<div class="select selector1">
		<select id="selectProductSort" class="selectProductSort form-control">
			<option value="position:asc" selected="selected">Sort by</option>
							<option value="price:asc" >Price: Lowest first</option>
				<option value="price:desc" >Price: Highest first</option>
						<option value="name:asc" >Product Name: A to Z</option>
			<option value="name:desc" >Product Name: Z to A</option>
							<option value="quantity:desc" >In stock</option>
						<option value="reference:asc" >Reference: Lowest first</option>
			<option value="reference:desc" >Reference: Highest first</option>
		</select>
	</div>
</form>
<!-- /Sort products -->



                </div>
			</div>
								</div><!-- #center_column -->

					</div><!-- .row -->


				</div>
			</div>

@include('front.layouts.footer-semi')