@include('front.layouts.head-semi')
			<div class="columns-container">
				<div id="columns" class="container">
											
<!-- Breadcrumb -->
            <div class="breadcrumb clearfix">
	<a class="home" href="http://kute-themes.com/prestashop/supershop/option1/" title="Return to Home">Home</a>
			<span class="navigation-pipe" >&nbsp;</span>
					<a href="12-fashion.html" title="Fashion" data-gg="">Fashion</a><span class="navigation-pipe">></span><span class="navigation_page">Women</span>
			</div>
<!-- /Breadcrumb -->
					                    
					
					<div class="row">
												<div id="left_column" class="column col-xs-12 col-sm-3">


<!-- Block layered navigation module -->


<div id="layered_block_left" class="block">

	<p class="title_block">Filter selection</p>

	<div class="block_content">

		<form action="#" id="layered_form">

			<div>

				
				
					
						
							<div class="layered_filter ">

						
                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Categories</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_category_0"></a>

                            </span>

						</div>

						<ul id="ul_layered_category_0" class="col-lg-12 layered_filter_ul">

							
								
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_4" id="layered_category_4" value="4" />

											
											<label 
												for="layered_category_4"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-tops" data-rel="nofollow">Tops<span> (6)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_5" id="layered_category_5" value="5" />

											
											<label 
												for="layered_category_5"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-t_shirts" data-rel="nofollow">T-shirts<span> (2)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_8" id="layered_category_8" value="8" />

											
											<label 
												for="layered_category_8"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-dresses" data-rel="nofollow">Dresses<span> (5)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_9" id="layered_category_9" value="9" />

											
											<label 
												for="layered_category_9"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-casual_dresses" data-rel="nofollow">Casual Dresses<span> (1)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_10" id="layered_category_10" value="10" />

											
											<label 
												for="layered_category_10"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-evening_dresses" data-rel="nofollow">Evening Dresses<span> (3)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_11" id="layered_category_11" value="11" />

											
											<label 
												for="layered_category_11"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-summer_dresses" data-rel="nofollow">Summer Dresses<span> (5)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_45" id="layered_category_45" value="45" />

											
											<label 
												for="layered_category_45"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-bags_shoes" data-rel="nofollow">Bags  &amp; Shoes<span> (1)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
									
										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_category_48" id="layered_category_48" value="48" />

											
											<label 
												for="layered_category_48"
																																																>
												
																								 
													
												
																											<a href="3-women.html#categories-blouses" data-rel="nofollow">Blouses<span> (6)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
								
							
							
						</ul>

					</div>

					
				
					
						
							<div class="layered_price" style="display: none;">

						
                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Price</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_price_0"></a>

                            </span>

						</div>

						<ul id="ul_layered_price_0" class="col-lg-12 layered_filter_ul">

							
								
									<li>

									<label >

										Range:

									</label>

									<span id="layered_price_range"></span>

									<div class="layered_slider_container">

										<div class="layered_slider" id="layered_price_slider" data-type="price" data-format="2" data-unit="€"></div>

									</div>

									</li>

								
							
							
						</ul>

					</div>

					
				
					
						
							<div class="layered_filter color_class  size_class">

						
                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Color</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_id_attribute_group_3"></a>

                            </span>

						</div>

						<ul id="ul_layered_id_attribute_group_3" class="col-lg-12 layered_filter_ul color-group">

							
								
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#AAB2BD" type="button" name="layered_id_attribute_group_5" data-rel="5_3" id="layered_id_attribute_group_5"  style="background: #AAB2BD;" />

												
											
											<label 
												for="layered_id_attribute_group_5"
																									 
														class="layered_color " data-rel="5_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#CFC4A6" type="button" name="layered_id_attribute_group_6" data-rel="6_3" id="layered_id_attribute_group_6"  style="background: #CFC4A6;" />

												
											
											<label 
												for="layered_id_attribute_group_6"
																									 
														class="layered_color " data-rel="6_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#f5f5dc" type="button" name="layered_id_attribute_group_7" data-rel="7_3" id="layered_id_attribute_group_7"  style="background: #f5f5dc;" />

												
											
											<label 
												for="layered_id_attribute_group_7"
																									 
														class="layered_color " data-rel="7_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#ffffff" type="button" name="layered_id_attribute_group_8" data-rel="8_3" id="layered_id_attribute_group_8"  style="background: #ffffff;" />

												
											
											<label 
												for="layered_id_attribute_group_8"
																									 
														class="layered_color " data-rel="8_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#faebd7" type="button" name="layered_id_attribute_group_9" data-rel="9_3" id="layered_id_attribute_group_9"  style="background: #faebd7;" />

												
											
											<label 
												for="layered_id_attribute_group_9"
																									 
														class="layered_color " data-rel="9_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#E84C3D" type="button" name="layered_id_attribute_group_10" data-rel="10_3" id="layered_id_attribute_group_10"  style="background: #E84C3D;" />

												
											
											<label 
												for="layered_id_attribute_group_10"
																									 
														class="layered_color " data-rel="10_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#434A54" type="button" name="layered_id_attribute_group_11" data-rel="11_3" id="layered_id_attribute_group_11"  style="background: #434A54;" />

												
											
											<label 
												for="layered_id_attribute_group_11"
																									 
														class="layered_color " data-rel="11_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#C19A6B" type="button" name="layered_id_attribute_group_12" data-rel="12_3" id="layered_id_attribute_group_12"  style="background: #C19A6B;" />

												
											
											<label 
												for="layered_id_attribute_group_12"
																									 
														class="layered_color " data-rel="12_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#F39C11" type="button" name="layered_id_attribute_group_13" data-rel="13_3" id="layered_id_attribute_group_13"  style="background: #F39C11;" />

												
											
											<label 
												for="layered_id_attribute_group_13"
																									 
														class="layered_color " data-rel="13_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#5D9CEC" type="button" name="layered_id_attribute_group_14" data-rel="14_3" id="layered_id_attribute_group_14"  style="background: #5D9CEC;" />

												
											
											<label 
												for="layered_id_attribute_group_14"
																									 
														class="layered_color " data-rel="14_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#A0D468" type="button" name="layered_id_attribute_group_15" data-rel="15_3" id="layered_id_attribute_group_15"  style="background: #A0D468;" />

												
											
											<label 
												for="layered_id_attribute_group_15"
																									 
														class="layered_color " data-rel="15_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#F1C40F" type="button" name="layered_id_attribute_group_16" data-rel="16_3" id="layered_id_attribute_group_16"  style="background: #F1C40F;" />

												
											
											<label 
												for="layered_id_attribute_group_16"
																									 
														class="layered_color " data-rel="16_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#964B00" type="button" name="layered_id_attribute_group_17" data-rel="17_3" id="layered_id_attribute_group_17"  style="background: #964B00;" />

												
											
											<label 
												for="layered_id_attribute_group_17"
																									 
														class="layered_color " data-rel="17_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input class="color-option  " value="#FCCACD" type="button" name="layered_id_attribute_group_24" data-rel="24_3" id="layered_id_attribute_group_24"  style="background: #FCCACD;" />

												
											
											<label 
												for="layered_id_attribute_group_24"
																									 
														class="layered_color " data-rel="24_3"
																																				>
												
																																				
																								
											</label>

										</li>

										
									
								
							
							
						</ul>

					</div>

					
				
					
						
							<div class="layered_filter  manufacturer_class">

						
                        <div class="layered_subtitle_heading">

                            <span class="layered_subtitle">Manufacturer</span>

                            <span class="layered_close">

                            	<a href="#" data-rel="ul_layered_manufacturer_0"></a>

                            </span>

						</div>

						<ul id="ul_layered_manufacturer_0" class="col-lg-12 layered_filter_ul">

							
								
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_manufacturer_15" id="layered_manufacturer_15" value="15" />

											
											<label 
												for="layered_manufacturer_15"
																																																>
												
																								 
													
												
																											<a href="3-women.html#manufacturer-channelo" data-rel="nofollow">Channelo<span> (3)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_manufacturer_21" id="layered_manufacturer_21" value="21" />

											
											<label 
												for="layered_manufacturer_21"
																																																>
												
																								 
													
												
																											<a href="3-women.html#manufacturer-mamypokon" data-rel="nofollow">Mamypokon<span> (2)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_manufacturer_20" id="layered_manufacturer_20" value="20" />

											
											<label 
												for="layered_manufacturer_20"
																																																>
												
																								 
													
												
																											<a href="3-women.html#manufacturer-pamperson" data-rel="nofollow">Pamperson<span> (1)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_manufacturer_22" id="layered_manufacturer_22" value="22" />

											
											<label 
												for="layered_manufacturer_22"
																																																>
												
																								 
													
												
																											<a href="3-women.html#manufacturer-pradano" data-rel="nofollow">Pradano<span> (5)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
										
										<li class="nomargin hiddable col-lg-6 ">

											
												<input type="checkbox" class="checkbox" name="layered_manufacturer_30" id="layered_manufacturer_30" value="30" />

											
											<label 
												for="layered_manufacturer_30"
																																																>
												
																								 
													
												
																											<a href="3-women.html#manufacturer-pumano" data-rel="nofollow">Pumano<span> (1)</span></a>
																										
																								
																								
											</label>

										</li>

										
									
								
							
							
						</ul>

					</div>

					
				
			</div>

			<input type="hidden" name="id_category_layered" value="3" />

			
				
			
				
			
				
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
						
					
				
			
				
			
		</form>

	</div>

	<div id="layered_ajax_loader" style="display: none;">

		<p>

			<img src="{{asset('front-end/img/loader.gif')}}" alt="" />

			<br />Loading...

		</p>

	</div>

</div>


<!-- /Block layered navigation module -->




<div id="htmlcontent_left">
	<ul class="htmlcontent-home clearfix row">
									<li class="htmlcontent-item-1 col-xs-12">
																					<img src="{{asset('front-end/modules/themeconfigurator/img/cfa4c95287277d00d81531c29169b6fa21e46a1e_banner.png')}}" class="item-img img-responsive" title="" alt=""  />
																											</li>
			</ul>
</div>

	<!-- Block CMS module -->

	
		<section id="informations_block_left_1" class="block informations_block_left">

			<h2 class="title_block">

				<a href="content/category/1-home.html">

					Information
				</a>

			</h2>

			<div class="block_content list-block">

				<ul>

					
					
						
							<li>

								<a href="content/1-delivery.html" title="Delivery">

									Delivery

								</a>

							</li>

						
					
						
							<li>

								<a href="content/2-legal-notice.html" title="Legal Notice">

									Legal Notice

								</a>

							</li>

						
					
						
							<li>

								<a href="content/3-terms-and-conditions-of-use.html" title="Terms and conditions of use">

									Terms and conditions of use

								</a>

							</li>

						
					
						
							<li>

								<a href="content/4-about-us.html" title="About us">

									About us

								</a>

							</li>

						
					
						
							<li>

								<a href="content/5-secure-payment.html" title="Secure payment">

									Secure payment

								</a>

							</li>

						
					
					
				</ul>

			</div>

		</section>

	
	<!-- /Block CMS module -->


</div>
																		<div id="center_column" class="center_column col-xs-12 col-sm-9">
	

	        <div id="responsive_slides">
    <div class="callbacks_container clearBoth">
      <ul id="categoryslider">
        	                       <li><img class="img-responsive" src="{{asset('front-end/modules/categoryslider/images/e515bcb0f12c53cc937ed819c12d3d73.jpg')}}" alt="Women Fashion 2015"  /></li>
                   	                       <li><img class="img-responsive" src="{{asset('front-end/modules/categoryslider/images/67a0a2a274e6c8424d20d14b85d06249.jpg')}}" alt="Women Fashion 2015"  /></li>
                         </ul>
    </div>
</div>


                    
                        
		<!-- Subcategories -->
		<div id="subcategories">
			<ul class="clearfix">
							<li>
					<a href="4-tops.html" title="Tops" >Tops</a>
				</li>
							<li>
					<a href="8-dresses.html" title="Dresses" >Dresses</a>
				</li>
							<li>
					<a href="45-bags-shoes.html" title="Bags  &amp; Shoes" >Bags  & Shoes</a>
				</li>
							<li>
					<a href="46-pants.html" title="Pants" >Pants</a>
				</li>
							<li>
					<a href="47-scaves.html" title="Scaves" >Scaves</a>
				</li>
							<li>
					<a href="48-blouses.html" title="Blouses" >Blouses</a>
				</li>
						</ul>
                            <div id="category_description_full" class="unvisible rte"><p><strong>You will find here all woman fashion collections.</strong></p>
<p>This category includes all the basics of your wardrobe and much more:</p>
<p>shoes, accessories, printed t-shirts, feminine dresses, women's jeans!</p></div>            
            		</div>
        
                
        
        <div class="view-product-list">
            <h1 class="page-heading product-listing"><span class="cat-name">Women&nbsp;</span></h1>
            <ul class="display hidden-xs">
	
    <li class="view_as_grid"><a rel="nofollow" href="#" title="Grid"><i class="icon-th-large"></i>Grid</a></li>
    <li class="view_as_list"><a rel="nofollow" href="#" title="List"><i class="icon-th-list"></i>List</a></li>
</ul>

        </div>
						
	                            								
		
	

    <input type="hidden" class="case-width" value="normal-width" />
	<!-- Products list -->
	<ul class="product_list grid row">
			
		
		
						
                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line last-item-of-mobile-line ">
            
                                                                                            			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/5-sexy-evening-dress.html" title="Sexy evening dress" itemprop="url">
							<img class="replace-2x img-responsive" src="{{asset('front-end/260-home_default/sexy-evening-dress.jpg')}}" alt="Printed Summer Dress" title="Printed Summer Dress"  width="230" height="276" itemprop="image" />
                            
						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										36,61 €
									</span>
                                                                        <span itemprop="price" class="price product-price">
										36,61 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />

									
									
															</div>
																			<a class="new-box" href="women/5-sexy-evening-dress.html">
								<span class="new-label">New</span>
							</a>
																			<a class="sale-box" href="women/5-sexy-evening-dress.html">
								<span class="sale-label">Sale!</span>
							</a>
						                        
                        <div class="functional-buttons clearfix">
                        
<div class="wishlist">
	<a class="addToWishlist wishlistProd_5" title="Add to my wishlist" href="javascript:void(0);" data-wl="5" onclick="WishlistCart('wishlist_block_list', 'add', '5', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>                    
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/5-sexy-evening-dress.html" data-id-product="5"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/5-sexy-evening-dress.html" rel="women/5-sexy-evening-dress.html">
    							<i class="fa fa-search"></i>
    						</a>
						                        
                        
                        
                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order62d3.html?add=1&amp;id_product=5&amp;ipa=2232&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="2232" data-id-product="5" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
													                        
                             
    					</div> 
					</div>
					
					
                    
                                      
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/5-sexy-evening-dress.html" title="Sexy evening dress" itemprop="url" >
							Sexy evening dress
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Long printed dress with thin
					</p>
                    <p class="product-desc-list" itemprop="description">
						Long printed dress with thin adjustable straps. V-neckline and wiring under the bust with ruffles at the bottom of the dress.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								36,61 €							  </span>
                            
							
							
											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "3.7" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">3</span> s)</span>		
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																					</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#671114789</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
			
		
		
						
                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line last-item-of-mobile-line ">
            
                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/6-blue-night-dress.html" title="Blue night dress" itemprop="url">
							<img class="replace-2x img-responsive" src="{{asset('front-end/241-home_default/blue-night-dress.jpg')}}" alt="Blue night dress" title="Blue night dress"  width="230" height="276" itemprop="image" />
                            
						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										36,60 €
									</span>
                                                                        <span itemprop="price" class="price product-price">
										36,60 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />

									
									
															</div>
																			<a class="new-box" href="women/6-blue-night-dress.html">
								<span class="new-label">New</span>
							</a>
												                        
                        <div class="functional-buttons clearfix">
                        
<div class="wishlist">
	<a class="addToWishlist wishlistProd_6" title="Add to my wishlist" href="javascript:void(0);" data-wl="6" onclick="WishlistCart('wishlist_block_list', 'add', '6', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>                    
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/6-blue-night-dress.html" data-id-product="6"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/6-blue-night-dress.html" rel="women/6-blue-night-dress.html">
    							<i class="fa fa-search"></i>
    						</a>
						                        
                        
                        
                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="ordere327.html?add=1&amp;id_product=6&amp;ipa=2260&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="2260" data-id-product="6" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
													                        
                             
    					</div> 
					</div>
					
					
                    
                                      
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/6-blue-night-dress.html" title="Blue night dress" itemprop="url" >
							Blue night dress
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Sleeveless knee-length chiffon
					</p>
                    <p class="product-desc-list" itemprop="description">
						Sleeveless knee-length chiffon dress. V-neckline with elastic under the bust lining.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								36,60 €							  </span>
                            
							
							
											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "4.7" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">6</span> s)</span>		
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																														</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#123632232</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
			
		
		
						
                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line first-item-of-tablet-line last-item-of-mobile-line ">
            
                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/7-sexy-women-blouse.html" title="Sexy women blouse" itemprop="url">
							<img class="replace-2x img-responsive" src="{{asset('front-end/221-home_default/sexy-women-blouse.jpg')}}" alt="Printed Chiffon Dress" title="Printed Chiffon Dress"  width="230" height="276" itemprop="image" />
                            
						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										24,60 €
									</span>
                                                                            
										 
											<span class="price-percent-reduction">20%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										19,68 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />

									
									
															</div>
																			<a class="new-box" href="women/7-sexy-women-blouse.html">
								<span class="new-label">New</span>
							</a>
												                        
                        <div class="functional-buttons clearfix">
                        
<div class="wishlist">
	<a class="addToWishlist wishlistProd_7" title="Add to my wishlist" href="javascript:void(0);" data-wl="7" onclick="WishlistCart('wishlist_block_list', 'add', '7', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>                    
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/7-sexy-women-blouse.html" data-id-product="7"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/7-sexy-women-blouse.html" rel="women/7-sexy-women-blouse.html">
    							<i class="fa fa-search"></i>
    						</a>
						                        
                        
                        
                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order0f19.html?add=1&amp;id_product=7&amp;ipa=2278&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="2278" data-id-product="7" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
													                        
                             
    					</div> 
					</div>
					
					
                    
                                      
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/7-sexy-women-blouse.html" title="Sexy women blouse" itemprop="url" >
							Sexy women blouse
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Printed chiffon knee length dress
					</p>
                    <p class="product-desc-list" itemprop="description">
						Printed chiffon knee length dress with tank straps. Deep v-neckline.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								19,68 €							  </span>
                            								
								<span class="old-price product-price">
									24,60 €
								</span>
								
																	<span class="price-percent-reduction">20%<span>OFF</span></span>
															
							
							
											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "5" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">4</span> s)</span>		
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#123689833</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
			
		
		
						
                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line last-line last-item-of-tablet-line last-item-of-mobile-line ">
            
                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="fashion/8-red-craft.html" title="Red craft" itemprop="url">
							<img class="replace-2x img-responsive" src="{{asset('front-end/86-home_default/red-craft.jpg')}}" alt="Women&#039;s Woolen" title="Women&#039;s Woolen"  width="230" height="276" itemprop="image" />
                            
						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										19,81 €
									</span>
                                                                            
										 
											<span class="price-percent-reduction">20%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										15,85 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />

									
									
															</div>
																			<a class="new-box" href="fashion/8-red-craft.html">
								<span class="new-label">New</span>
							</a>
																			<a class="sale-box" href="fashion/8-red-craft.html">
								<span class="sale-label">Sale!</span>
							</a>
						                        
                        <div class="functional-buttons clearfix">
                        
<div class="wishlist">
	<a class="addToWishlist wishlistProd_8" title="Add to my wishlist" href="javascript:void(0);" data-wl="8" onclick="WishlistCart('wishlist_block_list', 'add', '8', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>                    
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="fashion/8-red-craft.html" data-id-product="8"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="fashion/8-red-craft.html" rel="fashion/8-red-craft.html">
    							<i class="fa fa-search"></i>
    						</a>
						                        
                        
                        
                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="orderc4c4.html?add=1&amp;id_product=8&amp;ipa=1856&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="1856" data-id-product="8" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
													                        
                             
    					</div> 
					</div>
					
					
                    
                                      
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="fashion/8-red-craft.html" title="Red craft" itemprop="url" >
							Red craft
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Quisque malesuada placerat nisl.
					</p>
                    <p class="product-desc-list" itemprop="description">
						Quisque malesuada placerat nisl. Cras dapibus. Vestibulum ullamcorper mauris at ligula. Praesent adipiscing. Nullam cursus lacinia erat.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								15,85 €							  </span>
                            								
								<span class="old-price product-price">
									19,81 €
								</span>
								
																	<span class="price-percent-reduction">20%<span>OFF</span></span>
															
							
							
											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "0" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">1</span> s)</span>		
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																					</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#12908675</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
			
		
		
						
                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-line first-item-of-tablet-line last-item-of-mobile-line ">
            
                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="fashion/9-fashion-hand-bag.html" title="Fashion hand bag" itemprop="url">
							<img class="replace-2x img-responsive" src="{{asset('front-end/88-home_default/fashion-hand-bag.jpg')}}" alt="Women&#039;s Woolen2" title="Women&#039;s Woolen2"  width="230" height="276" itemprop="image" />
                            
						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										19,81 €
									</span>
                                                                            
										 
											<span class="price-percent-reduction">30%<span>OFF</span></span>
																			                                    <span itemprop="price" class="price product-price">
										13,87 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />

									
									
															</div>
																			<a class="new-box" href="fashion/9-fashion-hand-bag.html">
								<span class="new-label">New</span>
							</a>
												                        
                        <div class="functional-buttons clearfix">
                        
<div class="wishlist">
	<a class="addToWishlist wishlistProd_9" title="Add to my wishlist" href="javascript:void(0);" data-wl="9" onclick="WishlistCart('wishlist_block_list', 'add', '9', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>                    
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="fashion/9-fashion-hand-bag.html" data-id-product="9"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="fashion/9-fashion-hand-bag.html" rel="fashion/9-fashion-hand-bag.html">
    							<i class="fa fa-search"></i>
    						</a>
						                        
                        
                        
                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="ordere04f.html?add=1&amp;id_product=9&amp;ipa=1835&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="1835" data-id-product="9" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
													                        
                             
    					</div> 
					</div>
					
					
                    
                                      
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="fashion/9-fashion-hand-bag.html" title="Fashion hand bag" itemprop="url" >
							Fashion hand bag
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Aliquam lorem ante, dapibus in,
					</p>
                    <p class="product-desc-list" itemprop="description">
						Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Ut leo. Ut non enim eleifend felis pretium feugiat. Vivamus aliquet elit ac nisl. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede.
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								13,87 €							  </span>
                            								
								<span class="old-price product-price">
									19,81 €
								</span>
								
																	<span class="price-percent-reduction">30%<span>OFF</span></span>
															
							
							
											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star star_on"></div>
												<div class="star"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "3" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">1</span> s)</span>		
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#123654367</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
			
		
		
						
                                                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line last-line last-item-of-tablet-line last-item-of-mobile-line last-mobile-line ">
            
                                                                                                                        			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="women/25-faded-short-sleeve-tshirts.html" title="Modern long blouse" itemprop="url">
							<img class="replace-2x img-responsive" src="{{asset('front-end/92-home_default/faded-short-sleeve-tshirts.jpg')}}" alt="Women&#039;s Woolen2" title="Women&#039;s Woolen2"  width="230" height="276" itemprop="image" />
                            
						</a>
													<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
																	<span class="old-price product-price">
										19,81 €
									</span>
                                                                        <span itemprop="price" class="price product-price">
										19,81 €									</span>
									<meta itemprop="priceCurrency" content="EUR" />

									
									
															</div>
																			<a class="new-box" href="women/25-faded-short-sleeve-tshirts.html">
								<span class="new-label">New</span>
							</a>
												                        
                        <div class="functional-buttons clearfix">
                        
<div class="wishlist">
	<a class="addToWishlist wishlistProd_25" title="Add to my wishlist" href="javascript:void(0);" data-wl="25" onclick="WishlistCart('wishlist_block_list', 'add', '25', false, 1); return false;">
		<i class="fa-heart-o"></i>
	</a>
</div>                    
													<div class="compare">
								<a class="add_to_compare" title="Add to compare" href="women/25-faded-short-sleeve-tshirts.html" data-id-product="25"><i class="fa-compress"></i></a>
							</div>
						                            						<a class="quick-view" title="Quick View" href="women/25-faded-short-sleeve-tshirts.html" rel="women/25-faded-short-sleeve-tshirts.html">
    							<i class="fa fa-search"></i>
    						</a>
						                        
                        
                        
                        																							<a class="button ajax_add_to_cart_button btn btn-default" href="order9e10.html?add=1&amp;id_product=25&amp;ipa=1775&amp;token=0c49004b1215983386ec4e017795d1b5" rel="nofollow" title="Add to cart" data-id-product-attribute="1775" data-id-product="25" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
													                        
                             
    					</div> 
					</div>
					
					
                    
                                      
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="women/25-faded-short-sleeve-tshirts.html" title="Modern long blouse" itemprop="url" >
							Modern long blouse
						</a>
					</h5>
					<p class="product-desc" itemprop="description">
						Faded short sleeve t-shirt with high
					</p>
                    <p class="product-desc-list" itemprop="description">
						Faded short sleeve t-shirt with high neckline. Soft and stretchy material for a comfortable fit. Accessorize with a straw hat and you're ready for summer!
					</p>
										<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
													<meta itemprop="priceCurrency" content="EUR" />
		                      <span itemprop="price" class="price product-price">
    								19,81 €							  </span>
                            
							
							
											</div>
					                    <div class="comments_note" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<div class="star_content clearfix">
									<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
												<div class="star"></div>
							<meta itemprop="worstRating" content = "0" />
		<meta itemprop="ratingValue" content = "0" />
		<meta itemprop="bestRating" content = "5" />
	</div>			<span style="display:none" class="nb-comments">&nbsp;&nbsp;(<span itemprop="reviewCount">0</span> s)</span>		
</div>

											<div class="color-list-container"><ul class="color_to_pick_list clearfix">
	</ul></div>
										<div class="product-flags">
																														</div>
                                            <span class="itemcode">Item Code: <span class="itemcode-value">#54654632434</span></span>
                    																		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
                                <span class="availability-text">Availability:</span>
																	<span class="available-now">
										<link itemprop="availability" href="http://schema.org/InStock" />In stock									</span>
															</span>
															</div>
			</div><!-- .product-container> -->
		</li>
		</ul>





			<div class="content_sortPagiBar">
                <div class="sortPagiBar clearfix">
                    	<form method="post" action="http://kute-themes.com/prestashop/supershop/option1/en/products-comparison" class="compare-form">
		<button type="submit" class="btn btn-default button button-medium bt_compare bt_compare" disabled="disabled">
			<span>Compare (<strong class="total-compare-val">0</strong>)<i class="icon-chevron-right right"></i></span>
		</button>
		<input type="hidden" name="compare_product_count" class="compare_product_count" value="0" />
		<input type="hidden" name="compare_product_list" class="compare_product_list" value="" />
	</form>
			
		
		
		
	
                    
	
												<!-- Pagination -->
	<div id="pagination_bottom" class="pagination clearfix">
	    			<form class="showall" action="http://kute-themes.com/prestashop/supershop/option1/en/3-women" method="get">
				<div>
											                <button type="submit" class="btn btn-default button exclusive-medium">
	                	<span>Show all</span>
	                </button>
																																							<input type="hidden" name="id_category" value="3" />
																			                <input name="n" id="nb_item" class="hidden" value="13" />
				</div>
			</form>
							<ul class="pagination">
									<li id="pagination_previous_bottom" class="disabled pagination_previous">
						<span>
							<i class="fa fa-angle-double-left"></i>&nbsp;<b>Previous</b>
						</span>
					</li>
																															<li class="active current">
							<span>
								<span>1</span>
							</span>
						</li>
																				<li>
							<a  href="3-women905b.html?p=2">
								<span>2</span>
							</a>
						</li>
																				<li>
							<a  href="3-women2207.html?p=3">
								<span>3</span>
							</a>
						</li>
																																			<li id="pagination_next_bottom" class="pagination_next">
						<a  href="3-women905b.html?p=2">
							<b>Next</b> &nbsp;<i class="fa fa-angle-double-right"></i>
						</a>
					</li>
							</ul>
			</div>
    <div class="product-count">
    	    		                        	                        	Showing 1 - 6 of 13 items
		    </div>
	<!-- /Pagination -->

                    							<!-- nbr product/page -->
			<form action="http://kute-themes.com/prestashop/supershop/option1/en/3-women" method="get" class="nbrItemPage">
			<div class="clearfix selector1">
								                
																																	<input type="hidden" name="id_category" value="3" />
																			<select name="n" id="nb_item" class="form-control">
																							<option value="6" selected="selected">Show&nbsp;&nbsp;6</option>
																														<option value="12" >Show&nbsp;&nbsp;12</option>
																														<option value="30" >Show&nbsp;&nbsp;30</option>
																					</select>
				
			</div>
		</form>
		<!-- /nbr product/page -->

                    

<form id="productsSortForm" action="http://kute-themes.com/prestashop/supershop/option1/en/3-women" class="productsSortForm">
	<div class="select selector1">
		<select id="selectProductSort" class="selectProductSort form-control">
			<option value="position:asc" selected="selected">Sort by</option>
							<option value="price:asc" >Price: Lowest first</option>
				<option value="price:desc" >Price: Highest first</option>
						<option value="name:asc" >Product Name: A to Z</option>
			<option value="name:desc" >Product Name: Z to A</option>
							<option value="quantity:desc" >In stock</option>
						<option value="reference:asc" >Reference: Lowest first</option>
			<option value="reference:desc" >Reference: Highest first</option>
		</select>
	</div>
</form>
<!-- /Sort products -->
			
	
                    
                </div>
			</div>
								</div><!-- #center_column -->
					                    
					</div><!-- .row -->
                        
                    
				</div>
			</div>
										<!-- Footer -->
				<div class="footer-container">
					<footer id="footer">
						<!-- module advance footer by ovic-->
    <div id="advancefooter" class=" clearBoth clearfix container-fluid">
                        <div id="footer_row5" class="clearfix footer_row logo_footer_row">
                <div class="container">
                   <div class="row">
                                                                        <div id="block_5_1" class="logo_footer advancefooter-block col-sm-12 col-sx-12 block_1">
                                                                                                    <ul>
                                                                                    <li class="item html">
                                                <div class="item_wrapper">
                                                    <div id="footer_logo_block"><img src="{{asset('front-end/img/cms/logo_footer.png')}}" height="45" width="142" />
<div class="link_list_footer"><a href="#">Online Shopping</a> <a href="#">Buy</a> <a href="#">Sell</a> <a href="#">All Promotions</a> <a href="#">My Orders</a> <a href="#">Help</a> <a href="#">Site Map</a> <a href="#">Customer Service</a> <a href="#">About</a> <a href="#">Contact</a></div>
</div>
                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                                 </div>
                </div>
            </div>
                    <div id="footer_row2" class="clearfix footer_row row_footer_info">
                <div class="container">
                   <div class="row">
                                                                        <div id="block_2_1" class="horizontal-list advancefooter-block col-sm-8 col-sx-12 block_1">
                                                                                                    <ul>
                                                                                    <li class="item html">
                                                <div class="item_wrapper">
                                                    <div class="footer-block">
<h4 class="title_block mainFont">Customer Service</h4>
<ul class="toggle-footer bullet">
<li><a href="#" title="">Ask in Forum</a></li>
<li><a href="#" title="">Help Desk</a></li>
<li><a href="#" title="">Payment Methods</a></li>
<li><a href="#" title="">Custom Work</a></li>
<li><a href="#" title="">Promotions</a></li>
</ul>
</div>
                                                </div>
                                            </li>
                                                                                    <li class="item module">
                                                <div class="item_wrapper">
                                                    <!-- Block myaccount module -->
<section id="myaccount-footer" class="footer-block">
	<h4 class="title_block mainFont"><a href="loginfd9a.html" title="Manage my customer account" rel="nofollow">My account</a></h4>
	<div class="block_content toggle-footer">
		<ul class="bullet">
			<li><a href="login856b.html" title="My orders" rel="nofollow">My orders</a></li>
						<li><a href="login2f9b.html" title="My credit slips" rel="nofollow">My credit slips</a></li>
			<li><a href="login9f3d.html" title="My addresses" rel="nofollow">My addresses</a></li>
			<li><a href="login2432.html" title="Manage my personal information" rel="nofollow">My personal info</a></li>
						
            		</ul>
	</div>
</section>
<!-- /Block myaccount module -->

                                                </div>
                                            </li>
                                                                                    <li class="item module">
                                                <div class="item_wrapper">
                                                    
	<!-- MODULE Block footer -->

	<section class="footer-block" id="block_various_links_footer">

		<h4 class="title_block mainFont">Information</h4>

		<ul class="toggle-footer bullet">

			
			
			
			
			
			
				
					<li class="item">

						<a href="content/1-delivery.html" title="Delivery">

							Delivery

						</a>

					</li>

				
			
				
					<li class="item">

						<a href="content/2-legal-notice.html" title="Legal Notice">

							Legal Notice

						</a>

					</li>

				
			
				
					<li class="item">

						<a href="content/3-terms-and-conditions-of-use.html" title="Terms and conditions of use">

							Terms and conditions of use

						</a>

					</li>

				
			
				
					<li class="item">

						<a href="content/4-about-us.html" title="About us">

							About us

						</a>

					</li>

				
			
				
					<li class="item">

						<a href="content/5-secure-payment.html" title="Secure payment">

							Secure payment

						</a>

					</li>

				
			
			
		</ul>

		

	</section>

	<!-- /MODULE Block footer -->



                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                    <div id="block_2_2" class=" advancefooter-block col-sm-4 col-sx-12 block_2">
                                                                                                    <ul>
                                                                                    <li class="item module">
                                                <div class="item_wrapper">
                                                    <!-- MODULE Block contact infos -->
<section id="block_contact_infos" class="footer-block">
	<div>
        <h4 class="title_block mainFont">Contact Us</h4>
        <ul class="toggle-footer">
                        	<li>
            		<i class="icon-home"></i>My Company, 42 avenue des Champs Elysées
75000 Paris
France            	</li>
                                    	<li>
            		<i class="icon-mobile-phone"></i>Call us now: 
            		<span>0123-456-789</span>
            	</li>
                                    	<li>
            		<i class="icon-envelope"></i>Email: 
            		<span><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%73%61%6c%65%73@%79%6f%75%72%63%6f%6d%70%61%6e%79.%63%6f%6d" >&#x73;&#x61;&#x6c;&#x65;&#x73;&#x40;&#x79;&#x6f;&#x75;&#x72;&#x63;&#x6f;&#x6d;&#x70;&#x61;&#x6e;&#x79;&#x2e;&#x63;&#x6f;&#x6d;</a></span>
            	</li>
                    </ul>
    </div>
</section>
<!-- /MODULE Block contact infos -->

                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                                 </div>
                </div>
            </div>
                    <div id="footer_row1" class="clearfix footer_row ">
                <div class="container">
                   <div class="row">
                                                                        <div id="block_1_1" class=" advancefooter-block col-sm-6 col-sx-12 block_1">
                                                                                                    <ul>
                                                                                    <li class="item module">
                                                <div class="item_wrapper">
                                                    <!-- Block Newsletter module-->
<div id="newsletter_block_left" class="block">
	<h4 class="title_block mainFont">Newsletter</h4>
	<div class="block_content">
		<form action="http://kute-themes.com/prestashop/supershop/option1/en/" method="post">
			<div class="form-group" >
				<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" placeholder="Enter your email..." value="" />
                <button type="submit" name="submitNewsletter" class="btn btn-default button button-small">
                    
                    <i class="fa fa-envelope-o"></i>
                </button>
				<input type="hidden" name="action" value="0" />
			</div>
		</form>
	</div>
</div>
<!-- /Block Newsletter module-->

                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                    <div id="block_1_2" class=" advancefooter-block col-sm-6 col-sx-12 block_2">
                                                                                                    <ul>
                                                                                    <li class="item module">
                                                <div class="item_wrapper">
                                                    <section id="social_block">
    <h4 class="title_block mainFont">Follow us</h4>
	<ul class="clearfix">
        
					<li class="facebook">
				<a target="_blank" href="http://www.facebook.com/prestashop">
					<span>Facebook</span>
				</a>
			</li>
							<li class="twitter">
				<a target="_blank" href="http://www.twitter.com/prestashop">
					<span>Twitter</span>
				</a>
			</li>
				                	<li class="youtube">
        		<a target="_blank"  href="https://www.youtube.com/">
        			<span>Youtube</span>
        		</a>
        	</li>
                        	<li class="google-plus">
        		<a  target="_blank" href="https://www.google.com/+prestashop">
        			<span>Google Plus</span>
        		</a>
        	</li>
                        
                	</ul>
    
</section>
<div class="clearfix"></div>

                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                                 </div>
                </div>
            </div>
                    <div id="footer_row4" class="clearfix footer_row blocktags_footer">
                <div class="container">
                   <div class="row">
                                                                        <div id="block_4_1" class=" advancefooter-block col-sm-12 col-sx-12 block_1">
                                                                                                    <ul>
                                                                                    <li class="item module">
                                                <div class="item_wrapper">
                                                        <div id="tags_block_footer">
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #a6cada; color: #FFFFFF">
                    Fashion
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#a6cada"></span>
                					<span class="inner-tags">
                                            <a href="#">Shirts</a>
                                            <a href="#">Jeans</a>
                                            <a href="#">Kurtis</a>
                                            <a href="#">Sarees</a>
                                            <a href="#">Levis Jeans</a>
                                            <a href="#">Killer Jeans</a>
                                            <a href="#">Pepe Jeans</a>
                                            <a href="#">Arrow Shirts</a>
                                            <a href="#">Ethnic Wear</a>
                                            <a href="#">Formal Shirts</a>
                                            <a href="#">Peter England Shirts</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #ffd549; color: #FFF">
                    Furniture
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#ffd549"></span>
                					<span class="inner-tags">
                                            <a href="#">Living rooms</a>
                                            <a href="#">Sofa & couches</a>
                                            <a href="#">Beds</a>
                                            <a href="#">Chair</a>
                                            <a href="#">Coffee Table</a>
                                            <a href="#">Small Bench</a>
                                            <a href="#">Kitchen & Dining Room</a>
                                            <a href="#">Wayfarer</a>
                                            <a href="#">Library</a>
                                            <a href="#">Round</a>
                                            <a href="#">Shield-Oval</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #82a3cc; color: #FFFFFF">
                    Electronics
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#82a3cc"></span>
                					<span class="inner-tags">
                                            <a href="#">Mobile</a>
                                            <a href="#">Iphone</a>
                                            <a href="#">Lumia</a>
                                            <a href="#">Laptop</a>
                                            <a href="#">XPS Dell</a>
                                            <a href="#">Macbook</a>
                                            <a href="#">Samsung</a>
                                            <a href="#">Sony</a>
                                            <a href="#">Acer</a>
                                            <a href="#">Tablets</a>
                                            <a href="#">Apple</a>
                                            <a href="#">Samsung</a>
                                            <a href="#">Sony</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #c75347; color: #FFFFFF">
                    Food
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#c75347"></span>
                					<span class="inner-tags">
                                            <a href="#">Food Blogs</a>
                                            <a href="#">Foodies</a>
                                            <a href="#">Food Culture</a>
                                            <a href="#">Hashtag</a>
                                            <a href="#">Food Porn</a>
                                            <a href="#">Pizza</a>
                                            <a href="#">BBQ</a>
                                            <a href="#">Salad</a>
                                            <a href="#">Socola</a>
                                            <a href="#">Pate</a>
                                            <a href="#">Doner</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #59c6bb; color: #FFFFFF">
                    Sports
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#59c6bb"></span>
                					<span class="inner-tags">
                                            <a href="#">Football</a>
                                            <a href="#">Bikes</a>
                                            <a href="%40.html">Golf</a>
                                            <a href="#">Tennis</a>
                                            <a href="#">Karatedor</a>
                                            <a href="#">Yoga</a>
                                            <a href="#">Pencatsilat</a>
                                            <a href="#">Wushu</a>
                                            <a href="#">Runmeters</a>
                                            <a href="#">Boxing</a>
                                            <a href="#">Bowling</a>
                                            <a href="#">Gymnastics</a>
                                            <a href="#">Olympic</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #f59fba; color: #FFFFFF">
                    Jewelry
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#f59fba"></span>
                					<span class="inner-tags">
                                            <a href="#">Australian Opal</a>
                                            <a href="#">Ammolite</a>
                                            <a href="#">Meteorite Campo Del Cielo</a>
                                            <a href="#">Sun Pyrite</a>
                                            <a href="#">Faceted Carnelian</a>
                                            <a href="#">Round</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
            </div>

                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                                 </div>
                </div>
            </div>
                    <div id="footer_row3" class="clearfix footer_row ">
                <div class="container">
                   <div class="row">
                                                                        <div id="block_3_1" class="ft_copyright advancefooter-block col-sm-8 col-sx-12 block_1">
                                                                                                    <ul>
                                                                                    <li class="item html">
                                                <div class="item_wrapper">
                                                    &copy; 2015 Prestashop Demo SuperShop Online. All Rights Reserved.
                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                    <div id="block_3_2" class="payment_logo advancefooter-block col-sm-4 col-sx-12 block_2">
                                                                                                    <ul>
                                                                                    <li class="item html">
                                                <div class="item_wrapper">
                                                    <img src="{{asset('front-end/img/cms/payment_logo.png')}}" height="31" width="286" />
                                                </div>
                                            </li>
                                                                            </ul>
                                                            </div>
                                                                 </div>
                </div>
            </div>
                </div>
<!-- /advance footer by ovic-->    <div id="tags_block_footer">
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #a6cada; color: #FFFFFF">
                    Fashion
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#a6cada"></span>
                					<span class="inner-tags">
                                            <a href="#">Shirts</a>
                                            <a href="#">Jeans</a>
                                            <a href="#">Kurtis</a>
                                            <a href="#">Sarees</a>
                                            <a href="#">Levis Jeans</a>
                                            <a href="#">Killer Jeans</a>
                                            <a href="#">Pepe Jeans</a>
                                            <a href="#">Arrow Shirts</a>
                                            <a href="#">Ethnic Wear</a>
                                            <a href="#">Formal Shirts</a>
                                            <a href="#">Peter England Shirts</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #ffd549; color: #FFF">
                    Furniture
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#ffd549"></span>
                					<span class="inner-tags">
                                            <a href="#">Living rooms</a>
                                            <a href="#">Sofa & couches</a>
                                            <a href="#">Beds</a>
                                            <a href="#">Chair</a>
                                            <a href="#">Coffee Table</a>
                                            <a href="#">Small Bench</a>
                                            <a href="#">Kitchen & Dining Room</a>
                                            <a href="#">Wayfarer</a>
                                            <a href="#">Library</a>
                                            <a href="#">Round</a>
                                            <a href="#">Shield-Oval</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #82a3cc; color: #FFFFFF">
                    Electronics
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#82a3cc"></span>
                					<span class="inner-tags">
                                            <a href="#">Mobile</a>
                                            <a href="#">Iphone</a>
                                            <a href="#">Lumia</a>
                                            <a href="#">Laptop</a>
                                            <a href="#">XPS Dell</a>
                                            <a href="#">Macbook</a>
                                            <a href="#">Samsung</a>
                                            <a href="#">Sony</a>
                                            <a href="#">Acer</a>
                                            <a href="#">Tablets</a>
                                            <a href="#">Apple</a>
                                            <a href="#">Samsung</a>
                                            <a href="#">Sony</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #c75347; color: #FFFFFF">
                    Food
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#c75347"></span>
                					<span class="inner-tags">
                                            <a href="#">Food Blogs</a>
                                            <a href="#">Foodies</a>
                                            <a href="#">Food Culture</a>
                                            <a href="#">Hashtag</a>
                                            <a href="#">Food Porn</a>
                                            <a href="#">Pizza</a>
                                            <a href="#">BBQ</a>
                                            <a href="#">Salad</a>
                                            <a href="#">Socola</a>
                                            <a href="#">Pate</a>
                                            <a href="#">Doner</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #59c6bb; color: #FFFFFF">
                    Sports
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#59c6bb"></span>
                					<span class="inner-tags">
                                            <a href="#">Football</a>
                                            <a href="#">Bikes</a>
                                            <a href="%40.html">Golf</a>
                                            <a href="#">Tennis</a>
                                            <a href="#">Karatedor</a>
                                            <a href="#">Yoga</a>
                                            <a href="#">Pencatsilat</a>
                                            <a href="#">Wushu</a>
                                            <a href="#">Runmeters</a>
                                            <a href="#">Boxing</a>
                                            <a href="#">Bowling</a>
                                            <a href="#">Gymnastics</a>
                                            <a href="#">Olympic</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
                    <p class="category-tags">
                <span class="tags-title bluemess" style="background: #f59fba; color: #FFFFFF">
                    Jewelry
                </span>
                <span class="corner-icon bluemess" style="border-left-color:#f59fba"></span>
                					<span class="inner-tags">
                                            <a href="#">Australian Opal</a>
                                            <a href="#">Ammolite</a>
                                            <a href="#">Meteorite Campo Del Cielo</a>
                                            <a href="#">Sun Pyrite</a>
                                            <a href="#">Faceted Carnelian</a>
                                            <a href="#">Round</a>
                                            <a href="#">View all</a>
                    					</span>
                                
            </p>    
            </div>

					</footer>
				</div><!-- #footer -->
			            <a href="#" class="scroll_top" title="Scroll to Top">Scroll</a>
		</div><!-- #page -->
</body>
<!-- Mirrored from kute-themes.com/prestashop/supershop/option1/en/3-women by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 May 2018 06:04:22 GMT -->
</html>